
class XoLogic{

  List<List<bool?>> game = [];

  bool isDone = false;

  bool isX = true;

  XoLogic(){
    game = [
      for(int i = 0 ; i < 3 ; i++)
        [null,null,null,]
    ];
  }

  String get(int i , int j){
    if(game[i][j] != null) {
      if(game[i][j]!)
        return "X";
      else
        return "O";
    }
    return "";
  }

  void play(int i,int j){
    if(game[i][j] == null && ! isDone){
      game[i][j] = isX;
      isX = ! isX;
    }
  }

  String? check(){
    for(int i = 0 ; i < 3 ; i ++)
      if(game[0][i] == game[1][i] && game[1][i] == game[2][i] && game[1][i] != null)
          return game[1][i] == true ? 'X win' : 'O win';
      else if(game[i][0] == game[i][1] && game[i][1] == game[i][2] && game[i][1] != null)
          return game[i][0] == true ? 'X win' : 'O win';

    if(game[0][0] == game[1][1] && game[1][1] == game[2][2] && game[1][1] != null)
      return game[1][1] == true ? 'X win' : 'O win';

    if(game[0][2] == game[1][1] && game[1][1] == game[2][0] && game[1][1] != null)
      return game[1][1] == true ? 'X win' : 'O win';

    for(int i = 0 ; i < 3 ; i ++)
      for(int j = 0 ; j < 3 ; j ++)
        if(game[i][j] == null)
          return null;
        return 'Draw';
  }

}