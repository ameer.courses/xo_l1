import 'package:flutter/material.dart';
import 'logic.dart';

XoLogic logic = XoLogic();


class XoWidget extends StatefulWidget {

  final int i;
  final int j;

  XoWidget(this.i,this.j);

  @override
  State<XoWidget> createState() => _XoWidgetState();
}

class _XoWidgetState extends State<XoWidget> {


  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap:(){
          setState((){
            logic.play(widget.i, widget.j);
            var result = logic.check();
            if(result != null){
              logic.isDone = true;
              showDialog(
                  context: context,
                  builder: (context) =>
                      Dialog(
                        child: Container(
                            child: Text(result),
                          width: 200,
                          height: 100,
                          alignment: Alignment.center,
                        ),
                      )
              );
            }
          });
        },
        child: Container(
          width: double.infinity,
          height: double.infinity,
          margin: EdgeInsets.all(7.5),
          decoration: BoxDecoration(
            border: Border.all(width: 5,color: Colors.white),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(logic.get(widget.i, widget.j),
          style: TextStyle(
            color: Colors.white,
            fontSize: 26,
            fontWeight: FontWeight.bold
          ),
          ),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}



class XoRow extends StatelessWidget {

  final int i;
  XoRow(this.i);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Row(
        children: [
          for(int j = 0 ; j < 3 ; j++)
            XoWidget(i,j)
        ],
      ),
    );
  }
}



class XoBody extends StatefulWidget {

  @override
  State<XoBody> createState() => _XoBodyState();
}

class _XoBodyState extends State<XoBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.grey[400]!,
            Colors.black
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight
        )
      ),
      child: Column(
        children: [
          for(int i = 0 ; i < 3 ; i ++)
            XoRow(i),
          Expanded(
            child: IconButton(
                onPressed: press,
                icon: Icon(Icons.replay,color: Colors.white,)
            ),
          )
        ],
      ),
    );
  }

  void press(){
    setState((){
      logic = new XoLogic();
    });
  }
}
